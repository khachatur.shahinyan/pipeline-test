FROM python:latest
WORKDIR /app
COPY . /app
EXPOSE 8585
CMD ["python", "./server.py"]